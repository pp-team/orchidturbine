# ORCHIDTurbine

This folder contains the data to reproduce the ORCHID turbine geometry. 

## How to open the data files

The impeller geometry is provided in ".step" format and can be therefore opened with any CAD modeller.
The stator geometry is provided in ".crv" format. The ".crv" files can be directly imported in Ansys Turbogrid. Alternatively, the points of the blade profile can be easily extracted from the file profile_stator.crv and opened with other software.

## Support

In case of issues related to the turbine geometry or missing information please contact M.Pini@tudelft.nl. 
